const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
	products: [{
		productId: {
			type: String,
			required: [true, "Please specify the product you want."]
		},
		quantity: {
			type: Number,
			default: 1
		}
	}]
});



module.exports = mongoose.model("Cart", cartSchema);