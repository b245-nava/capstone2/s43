const express = require('express');
const router = express.Router();
const orderController = require('../Controllers/orderControllers.js');
const cartController = require('../Controllers/cartControllers.js');
const auth = require('../auth.js');

// [Order Routes]

// User checkout (Create Order)
// router.post('/checkout', auth.verify, orderController.checkout);

// [Cart Routes]

// Add specified product to cart
router.post('/addCart/:prodId', auth.verify, cartController.addCart);

module.exports = router;