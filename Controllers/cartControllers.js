const mongoose = require('mongoose');
const Cart = require('../Models/cartSchema.js');
const auth = require('../auth.js');

// Cart
/*
	Business Logic:
		- What is a "cart" for? It's for:
			- Holding and storing the user's chosen products
			- The user can reach in and add products to the cart, or remove products as they please
		- Since admins can't check out products, they shouldn't be able to use a cart either
		- A cart is automatically created for users when they select products to buy, and a user can have multiple carts
		- The cart is "returned" once the user checks out their products at the cashier
*/

// Controller for 
module.exports.addCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const prodId = request.params.prodId;
	const input = request.body;

	if(userData.isAdmin){
		return response.send('Please log into your user account to make purchases!')
	} else {

		Product.findById({_id: prodId})
		.then(result => {

			if(result === null){
				return response.send('Please double-check the product ID and try again.')
			} else {
				
			}
		})

	}
}