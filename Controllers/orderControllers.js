const mongoose = require('mongoose');
const Order = require('../Models/orderSchema.js');
const auth = require('../auth.js');

// Creating new order
/*
	Business Logic:
		1. Only users w/o admin access can create orders.
		2. An order must contain the ff:
			- userId of registered customer making order
			- list of ordered products (array of objects: indicate product ID and quantity of item)
			- user's address
			- total of order

		This means that:
			- UserId must be pushed onto order's records, while the order's ID can be associated with the user that made it
			- The chosen product's ID needs to be pushed onto the order form, and quantity needs to be indicated
			- The user needs to indicate the address to which their order will be delivered.

	Possible user-end errors:
		1. User is an admin
		2. Invalid product ID

	Possible code bugs:
		1. Non-existent product ID gets added into order anyway
		2. totalAmount does not automatically and precisely reflect the total price of the order
*/

module.exports.checkout = (request, response) => {

	const userData = auth.decode(request.headers.authorization); // to verify if user is admin and to extract userId
	const input = request.body; // product IDs, quantity, and address go here (follow orderSchema)

	if(userData.isAdmin){
		return response.send('Please log into your user account to make purchases!');
	} else {

/*		const userId = userData._id;

		let newOrder = new Order({
			userId: userData._id,
			products: [{
				productId: input.productId,
				quantity: input.quantity
			}],
			address: input.address,
			totalAmount: input.totalAmount
		});

		newOrder.save()
		.then(result => {
			console.log(result)
			return response.send(`Thank you for placing your order! Here is your receipt number: ${result._id}`)
		})
		.catch(err => {
			console.log(err)
			return response.send('Sorry, there was an error. Please try again!')
		});*/

	}

};